#!/usr/bin/env bash
# this script is automatically run from CMakeLists.txt

BUILD_ROOT=$PWD
TORCH_PREFIX=$PWD/torch

echo "[Pre-build]  dependency installer script running..."
echo "[Pre-build]  build root directory:       $BUILD_ROOT"


# break on errors
#set -e


# DetectNet's  (uncomment to download)
#wget --no-check-certificate 'https://nvidia.box.com/shared/static/xe6wo1o8qiqykfx8umuu0ki9idp0f92p.prototxt' -O detectnet.prototxt
#mv detectnet.prototxt ../data/networks
mkdir $BUILD_ROOT/ext/pkg-tensorrt/networks

wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=0BwYxpotGWRNOWXpOQ0JCQ3AxSTA' -O $BUILD_ROOT/ext/pkg-tensorrt/networks/facenet-120.tar.gz
tar -xzvf $BUILD_ROOT/ext/pkg-tensorrt/networks/facenet-120.tar.gz -C $BUILD_ROOT/ext/pkg-tensorrt/networks
rm $BUILD_ROOT/ext/pkg-tensorrt/networks/facenet-120.tar.gz

#wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=0BwYxpotGWRNOMzVRODNuSHlvbms' -O ped-100.tar.gz
#tar -xzvf ped-100.tar.gz -C ../data/networks

#wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=0BwYxpotGWRNOUmtGdGIyYjlEbTA' -O multiped-500.tar.gz
#tar -xzvf multiped-500.tar.gz -C ../data/networks

echo "[Pre-build]  Finished CMakePreBuild script"
