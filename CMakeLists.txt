
cmake_minimum_required(VERSION 2.8)

#Only linux_cuda tested, could be extended to cuda_host
if(NOT (${PLATFORM} MATCHES "linux_cuda_jp_3.3" OR ${PLATFORM} MATCHES "linux_cuda_jp_4.1"))
    return()
endif()
if(${PLATFORM} MATCHES "linux_cuda_jp_3.3")
    set(TENSOR_RT_VERSION "tensorRT_4.0")
elseif(${PLATFORM} MATCHES "linux_cuda_jp_4.1")
    set(TENSOR_RT_VERSION "tensorRT_5.0")
    set(TENSOR_RT_5_LIB "/tegra")
endif()

# setup tensorRT flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")	# -std=gnu++11
set(BUILD_DEPS "YES" CACHE BOOL "If YES, will install dependencies into sandbox.  Automatically reset to NO after dependencies are installed.")

# Target Libraries
set(CUDA_INSTALL_DIR "${CMAKE_SYSROOT}/usr/local/cuda")
set(CUDA_CUDART_LIBRARY "${CMAKE_SYSROOT}/usr/local/cuda/lib64/libcudart.so")
set(LIB "pkg_tensorrt")

# Host Libraries
set(CUDA_TOOLKIT_ROOT_DIR "/usr/local/cuda")
set(CUDA_TOOLKIT_INCLUDE "/usr/local/cuda/include")
set(CUDA_HOST_COMPILER  "/usr/local/cuda/bin/nvcc")

# if this is the first time running cmake, perform pre-build dependency install script (or if the user manually triggers re-building the dependencies)
if( ${BUILD_DEPS} )
	message("Launching pre-build dependency installer script...")

	execute_process(COMMAND sh ${CMAKE_CURRENT_LIST_DIR}/CMakePreBuild.sh
				WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    			RESULT_VARIABLE PREBUILD_SCRIPT_RESULT)

	set(BUILD_DEPS "NO" CACHE BOOL "If YES, will install dependencies into sandbox.  Automatically reset to NO after dependencies are installed." FORCE)
	message("Finished installing dependencies")
endif()

# setup CUDA
# We need CUDA Host compiler, therefore, we need to change root
set(CURRENT_TARGET_SYSROOT ${CMAKE_SYSROOT})
set(CMAKE_SYSROOT /)
find_package(CUDA)

set(
	CUDA_NVCC_FLAGS
	${CUDA_NVCC_FLAGS}; 
    #-O3 
	-gencode arch=compute_53,code=sm_53
	-gencode arch=compute_62,code=sm_62
)

if(CUDA_VERSION_MAJOR GREATER 9)
	message("-- CUDA ${CUDA_VERSION_MAJOR} detected, enabling SM_72")

	set(
		CUDA_NVCC_FLAGS
		${CUDA_NVCC_FLAGS}; 
		-gencode arch=compute_72,code=sm_72
	)
endif()
set(CMAKE_SYSROOT ${CURRENT_TARGET_SYSROOT})

# setup project output paths
set(PROJECT_OUTPUT_DIR  ${PROJECT_BINARY_DIR}/deps)
set(PROJECT_INCLUDE_DIR ${CMAKE_BINARY_DIR}/deps/include/tensorrt)

file(MAKE_DIRECTORY ${PROJECT_INCLUDE_DIR})
file(MAKE_DIRECTORY ${PROJECT_OUTPUT_DIR}/bin)

message("-- system arch:  ${CMAKE_SYSTEM_PROCESSOR}")
message("-- output path:  ${PROJECT_OUTPUT_DIR}")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_OUTPUT_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_OUTPUT_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_OUTPUT_DIR}/lib)

# build C/C++ interface
link_directories(
    ${CUDA_INSTALL_DIR}/targets/aarch64-linux/lib 
    ${CUDA_INSTALL_DIR}/lib64 
    ${CMAKE_CURRENT_LIST_DIR}/lib/${TENSOR_RT_VERSION}
    ${CMAKE_CURRENT_LIST_DIR}/lib/${TENSOR_RT_VERSION}${TENSOR_RT_5_LIB}
    ${CMAKE_SYSROOT}/usr/lib/aarch64-linux-gnu
    ${CMAKE_SYSROOT}/lib
    
)

include_directories(
    ${PROJECT_INCLUDE_DIR} 
    ${GIE_PATH}/include
    ${CMAKE_CURRENT_LIST_DIR}/include/${TENSOR_RT_VERSION})

file(GLOB inferenceSources 
    ${CMAKE_CURRENT_LIST_DIR}/src/*.cpp 
    ${CMAKE_CURRENT_LIST_DIR}/src/*.cu 
    ${CMAKE_CURRENT_LIST_DIR}/src/utils/commandLine.cpp 
    ${CMAKE_CURRENT_LIST_DIR}/src/utils/cuda/cudaOverlay.cu 
    ${CMAKE_CURRENT_LIST_DIR}/src/calibration/*.cpp
)
file(GLOB inferenceIncludes 
    ${CMAKE_CURRENT_LIST_DIR}/src/*.h 
    ${CMAKE_CURRENT_LIST_DIR}/src/utils/commandLine.h 
    ${CMAKE_CURRENT_LIST_DIR}/src/utils/cuda/*.h 
    ${CMAKE_CURRENT_LIST_DIR}/src/calibration/*h
    ${CMAKE_CURRENT_LIST_DIR}/include/${TENSOR_RT_VERSION}/*.h
)

cuda_add_library(${LIB} STATIC ${inferenceSources})
target_link_libraries(${LIB} nvcaffe_parser nvinfer cudnn cublas)


# transfer all headers to the include directory
foreach(include ${inferenceIncludes})
	configure_file(${include} ${PROJECT_INCLUDE_DIR} COPYONLY)
endforeach()

# create symbolic link for network data
#execute_process( COMMAND "${CMAKE_COMMAND}" "-E" "create_symlink" "${PROJECT_SOURCE_DIR}/data/networks" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/networks" )
   
# copy image data
file(GLOB libs ${CMAKE_CURRENT_LIST_DIR}/lib/${TENSOR_RT_VERSION}/*)

foreach(lib ${libs})
	file(COPY ${lib} DESTINATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
endforeach()


# build samples & utilities
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/src/docs)
#add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/src/utils)


# install
foreach(include ${inferenceIncludes})
    install(FILES "${include}" DESTINATION include/${LIB})
endforeach()

# install the shared library
install(TARGETS ${LIB} DESTINATION lib/${LIB} EXPORT ${LIB}Config)

# install the cmake project, for importing
install(EXPORT ${LIB}Config DESTINATION share/${LIB}/cmake)


