# pkg-tensorrt

Based on:

- [jetson-inference](https://github.com/dusty-nv/jetson-inference#locating-object-coordinates-using-detectnet)

Support for:

- TensorRT 4.0
- TensorRT 5.0


** lib/TensorRT libraries have been copied from Jetpack sources with the only aim of providing cross-compilation capabilities - No modification has been carried out
