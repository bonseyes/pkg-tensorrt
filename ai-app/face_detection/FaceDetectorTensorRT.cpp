/**
 * NVISO CONFIDENTIAL
 *
 * Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
 *
 * The source code contained or described herein and all documents related to
 * the source code ("Material") is the confidential and proprietary information
 * owned by nViso or its suppliers or licensors.  Title to the  Material remains
 * with nViso Sarl or its suppliers and licensors. The Material contains trade
 * secrets and proprietary and confidential information of nViso or its
 * suppliers
 * and licensors. The Material is protected by worldwide copyright and trade
 * secret laws and treaty provisions. You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with nViso.
 *
 * NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */

#include "FaceDetectorTensorRT.hpp"
#include "detectNet.h"

#include "cudaMappedMemory.h"
#include "cudaUtility.h"
#include <sys/time.h>

#include "plog/Log.h"
#include "nvisocv/imgproc.hpp"

#include <stdio.h>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <string>
#include <vector>


using namespace std;

namespace nviso {

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif
#define MAXNDETECTIONS 100000


// Internal Structure
struct FaceDetectorTensorRT::Private {
  string prototxt{"deploy.prototxt"};
  string model{"snapshot_iter_24000.caffemodel"};
  string input_blob{"data"};
  string output_blob{"coverage"}; 
  string output_boxes{"bboxes"};
  float threshold{0.5};
  float mean_pixel{0.0f};
  uint32_t batch_size{1};

  std::unique_ptr<detectNet> net_;

  float* bbCPU    = NULL;
  float* bbCUDA   = NULL;
  float* confCPU  = NULL;
  float* confCUDA = NULL; 

  uint32_t maxBoxes = 0;
  uint32_t classes = 0; 
};


uint64_t current_timestamp() {
  struct timeval te; 
  gettimeofday(&te, NULL); // get current time
  return te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
}

 
typedef std::tuple<float, float, float> vec3f;
static bool loadImageRGBA(const ncv::Mat& rgbImg, float4** cpu, float4** gpu, int* width, int* height )
{
  if( !cpu || !gpu || !width || !height )
  {
	printf("loadImageRGBA - invalid parameter\n");
	return false;
  }

  const uint32_t imgWidth  = rgbImg.cols();
  const uint32_t imgHeight = rgbImg.rows();
  const uint32_t imgPixels = imgWidth * imgHeight;
  const size_t   imgSize   = imgWidth * imgHeight * sizeof(float) * 4;

  printf("loaded image  (%u x %u)  %zu bytes\n", imgWidth, imgHeight, imgSize);

  // allocate buffer for the image
  if( !cudaAllocMapped((void**)cpu, (void**)gpu, imgSize) )
  {
	printf(LOG_CUDA "failed to allocated %zu bytes for image \n", imgSize);
	return false;
  }

  printf("\nafter");

  float4* cpuPtr = *cpu;

  std::vector<ncv::Mat> rgb_split(3);
  rgb_split = rgbImg.split();
  float* red = (float*)rgb_split[0].data();
  float* green = (float*)rgb_split[1].data();
  float* blue = (float*)rgb_split[2].data();
	
  for( uint32_t x=0; x < imgHeight; x++ )
  {
	for( uint32_t y=0; y < imgWidth; y++ )
	{
      //vec3f pixel = rgbImg.at<vec3f>(x, y);

	  const float4 px = make_float4(red[x*imgWidth+y], 
		 						    green[x*imgWidth+y], 
									blue[x*imgWidth+y],
									float(255));

	  cpuPtr[x*imgWidth+y] = px;
	}
  }
	
  *width  = imgWidth;
  *height = imgHeight;	
  return true;
}

/**
 *	Constructor
 *	@param
 */
FaceDetectorTensorRT::FaceDetectorTensorRT() : d(new Private()) {}

// Deconstructor
FaceDetectorTensorRT::~FaceDetectorTensorRT() { delete d; }

void FaceDetectorTensorRT::setMinMax(int minSize, int maxSize){
}


bool FaceDetectorTensorRT::setOptions(const json& cfg) {
  setValue(d->prototxt, cfg, "network");
  setValue(d->model, cfg, "model");
  setValue(d->input_blob, cfg, "input_blob");
  setValue(d->output_blob, cfg, "output_blob");
  setValue(d->output_boxes, cfg, "output_boxes");
  setValue(d->threshold, cfg, "threshold");
  return true;
}

bool FaceDetectorTensorRT::init(const json& cfg) {
  if(!FaceDetectorTensorRT::setOptions(cfg))
    printf("Options could not be set, using default options");  

  d->net_.reset(detectNet::Create(d->prototxt.c_str(), d->model.c_str(), d->mean_pixel, d->threshold, 
                                  d->input_blob.c_str(), d->output_blob.c_str(), d->output_boxes.c_str(), 1));
  if( !d->net_ ){
	printf("detectnet-console:   failed to initialize detectNet\n");
	return 0;
  }

  d->net_->EnableProfiler();
	
  // alloc memory for bounding box & confidence value output arrays
  d->maxBoxes = d->net_->GetMaxBoundingBoxes();		printf("maximum bounding boxes:  %u\n", d->maxBoxes);
  d->classes  = d->net_->GetNumClasses();
	

  if( !cudaAllocMapped((void**)& d->bbCPU, (void**)& d->bbCUDA, (const uint32_t) d->maxBoxes * sizeof(float4)) ||
	  !cudaAllocMapped((void**)& d->confCPU, (void**)& d->confCUDA,(const uint32_t) d->maxBoxes * (const uint32_t) d->classes * sizeof(float)) ){
	printf("detectnet-console:  failed to alloc output memory\n");
	return 0;
  }
	  
  return true;
}


bool FaceDetectorTensorRT::predict(const ncv::Image &inputImg, list<DetectedFace> *faces) {
  
  faces->clear();
  ncv::Mat rgbImg(ncv::ImageType::RGB);
  if (inputImg.channels() != 3) {
    return false;
  }
  // Assume input image is BGR, convert to RGB, float
  ncv::cvtColor(inputImg, rgbImg, ncv::ImageType::RGB);
  rgbImg.convertTo(rgbImg, NCV_32FC3);

  // load image from file on disk
  float* imgCPU    = NULL;
  float* imgCUDA   = NULL;
  int    imgWidth  = 0;
  int    imgHeight = 0;
	
  //const char* imgFilename = "myImage";	
  if( !loadImageRGBA(rgbImg, (float4**)& imgCPU, (float4**)&imgCUDA, &imgWidth, &imgHeight) ){
	printf("failed to load image \n");
	return 0;
  }
	
  // classify image
  int numBoundingBoxes = d->maxBoxes;
	
  printf("detectnet-console:  beginning processing network (%zu)\n", current_timestamp());

  const bool result = d->net_->Detect(imgCUDA, imgWidth, imgHeight, d->bbCPU, &numBoundingBoxes, d->confCPU);

  printf("detectnet-console:  finished processing network  (%zu)\n", current_timestamp());

  if( !result )
	printf("detectnet-console:  failed to classify \n");

  printf("%i bounding boxes detected\n", numBoundingBoxes);
	
  int lastClass = 0;
  int lastStart = 0;
	
  for( int n=0; n < numBoundingBoxes; n++ ){
    float confidence = d->confCPU[n*2+0];	
	const int nc = d->confCPU[n*2+1];
	float* bb = d->bbCPU + (n * 4);

   // Top left corner
    float x1 = bb[0];
    float y1 = bb[1];
    // Bottom right corner
    float x2 = bb[2];
    float y2 = bb[3];

    DetectedFace face = { confidence, {x1, y1, x2 - x1, y2 - y1}, Landmarks{} };
    faces->push_back(face);
		
	//printf("bounding box %i   (%f, %f)  (%f, %f)  w=%f  h=%f\n", n, bb[0], bb[1], bb[2], bb[3], bb[2] - bb[0], bb[3] - bb[1]); 
	
	/*if( nc != lastClass || n == (numBoundingBoxes - 1) ){
	  if( !d->net->DrawBoxes(imgCUDA, imgCUDA, imgWidth, imgHeight, d->bbCUDA + (lastStart * 4), (n - lastStart) + 1, lastClass) )
		printf("detectnet-console:  failed to draw boxes\n");
				
	  lastClass = nc;
	  lastStart = n;
	}*/
  }
	
  CUDA(cudaThreadSynchronize());
	

  //printf("\nshutting down...\n");
  CUDA(cudaFreeHost(imgCPU));
  detectNet* net = d->net_.release();
  delete net;

  return true;
}
}  // nviso
